# Contributing

Gundala `bitbucket.org/rctiplus/gundala` is an open-source project. 
It is licensed using the [Apache License 2.0][1]. 
We appreciate pull requests; here are our guidelines:

1.  [File an issue][2] 
    (if there isn't one already). If your patch
    is going to be large it might be a good idea to get the
    discussion started early.

2.  Please use [Effective Go Community Guidelines][3].

3.  We ask that you squash all the commits together before
    pushing and that your commit message references the bug.

## Issue Reporting
- Check that the issue has not already been reported.
- Be clear, concise and precise in your description of the problem.
- Open an issue with a descriptive title and a summary in grammatically correct,
  complete sentences.
- Include any relevant code to the issue summary.

## Pull Requests
- Use a topic branch to easily amend a pull request later, if necessary.
- Write [good commit messages][4].
- Use the same coding conventions as the rest of the project.
- Open a [pull request][5] that relates to *only* one subject with a clear title
  and description in grammatically correct, complete sentences.
  
  
[1]: http://www.apache.org/licenses/LICENSE-2.0
[2]: https://bitbucket.org/rctiplus/gundala/issues  
[3]: https://golang.org/doc/effective_go.html
[4]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
[5]: https://help.github.com/articles/using-pull-requests

