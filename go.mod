module bitbucket.org/rctiplus/gundala

go 1.15

require (
	github.com/jordan-wright/email v4.0.1-0.20200917010138-e1c00e156980+incompatible
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.20.0
)
