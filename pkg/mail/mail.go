package mail

import (
	"bitbucket.org/rctiplus/gundala/pkg/datatype/array"
	"bitbucket.org/rctiplus/gundala/pkg/datatype/integer"
	"github.com/jordan-wright/email"
	"github.com/rs/zerolog/log"
	"net/smtp"
)

type SmtpConfig struct {
	Name     string
	Username string
	Password string
	Hostname string
	Port     int
}

func (mc *SmtpConfig) Address() string {
	return mc.Hostname + `:` + integer.IntToString(mc.Port)
}

func (mc *SmtpConfig) Auth() smtp.Auth {
	return smtp.PlainAuth(``, mc.Username, mc.Password, mc.Hostname)
}
func (mc *SmtpConfig) From() string {
	return mc.Name + ` <` + mc.Username + `>`
}

// run sendbcc on another goroutine
func (mc *SmtpConfig) SendBCC(bcc []string, subject string, message string) {
	log.Print(`SendBCC started ` + array.StringJoin(bcc, `, `) + `; subject: ` + subject)
	err := make(chan error, 1)
	go func() {
		err <- mc.SendSyncBCC(bcc, subject, message)
		close(err)
	}()

	if err != nil {
		log.Err(<-err)
	}
}

// run sendAttachbcc on another goroutine
func (mc *SmtpConfig) SendAttachBCC(bcc []string, subject string, message string, files []string) {
	log.Print(`SendAttachBCC started ` + array.StringJoin(bcc, `, `) + `; subject: ` + subject)
	err := make(chan error, 1)
	go func() {
		err <- mc.SendSyncAttachBCC(bcc, subject, message, files)
		close(err)
	}()

	if err != nil {
		log.Err(<-err)
	}
}

// SendSyncBCC send BCC using synchronous approach
func (mc *SmtpConfig) SendSyncBCC(bcc []string, subject string, message string) error {
	return mc.SendSyncAttachBCC(bcc, subject, message, []string{})
}

// SendSyncAttachBCC send synchronous with attachment and BCC
func (mc *SmtpConfig) SendSyncAttachBCC(bcc []string, subject string, message string, files []string) error {
	e := email.NewEmail()
	e.From = mc.From()
	e.To = []string{e.From}
	e.Bcc = bcc
	e.Subject = subject
	attach := array.StringJoin(files, ` `)
	for _, file := range files {
		e.AttachFile(file)
	}

	if attach != `` {
		attach = `; attachments: ` + attach
	}

	e.HTML = []byte(message + e.From)
	err := e.Send(mc.Address(), mc.Auth())
	if err != nil {
		return err
	}

	log.Print(`SendAttachBCC completed ` + array.StringJoin(bcc, `, `) + attach + `; subject: ` + subject)
	return err
}
