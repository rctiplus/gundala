package encoder

import (
	"bytes"
	"encoding/json"
)

// EncodeUnicode - helper for encode the unicode
func EncodeUnicode(data interface{}) string {
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(false)
	_ = enc.Encode(data)
	return string(buf.String())
}

