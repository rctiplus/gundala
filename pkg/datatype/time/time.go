package time

import (
	"bitbucket.org/rctiplus/gundala/pkg/datatype/integer"
	"time"
)

const (
	ISO            = `2006-01-02T15:04:05.999999`
	YMD_HM         = `2006-01-02 15:04`
	YMD_HMS        = `2006-01-02 15:04:05`
	YMD            = `2006-01-02`
	FILE           = `20060102_150405`
	HUMAN_DATETIME = `2-Jan-2006 15:04:05`
	HUMAN_DATE     = `2 Jan 2006`
	YY             = `06`
	YMDH           = `20060102.15`
	YMDHM          = `20060102.1504`
	HMS            = `150405`
)


var EMPTY = time.Time{}

// ToIsoString convert time to iso formatted time string
// time.ToIsoString(time.Now()) // "2016-03-17T10:04:50.6489"
func ToIsoString(t time.Time) string {
	if t == EMPTY {
		return ``
	}
	return t.Format(ISO)
}

// IsoTime return current iso time
// time.IsoTime() // "2016-03-17T10:07:56.418728"
func IsoTime() string {
	return time.Now().Format(ISO)
}

// ToDateString convert time to iso date string
// time.ToDateString(time.Now()) // output "2016-03-17"
func ToDateString(t time.Time) string {
	if t == EMPTY {
		return ``
	}
	return t.Format(YMD)
}

// DateString return current iso date
// time.DateString()) // "2016-03-17"
func DateString() string {
	return time.Now().Format(YMD)
}

// ToHumanString convert time to human date
// time.ToHumanString(time.Now()) // "17-Mar-2016 10:06"
func ToHumanString(t time.Time) string {
	if t == EMPTY {
		return ``
	}
	return t.Format(HUMAN_DATETIME)
}

// HumanString return current human date
// time.HumanString() // "17-Mar-2016 10:06"
func HumanString() string {
	return time.Now().Format(HUMAN_DATETIME)
}

// ToDateHourString convert time to iso date and hour:minute
// time.ToDateHourString(time.Now()) // "2016-03-17 10:07"
func ToDateHourString(t time.Time) string {
	if t == EMPTY {
		return ``
	}
	return t.Format(YMD_HM)
}

// ToHhMmSs convert time to iso date and hourminutesecond
// time.ToHhMmSs(time.Now()) // "230744"
func ToHhMmSs(t time.Time) string {
	if t == EMPTY {
		return ``
	}
	return t.Format(HMS)
}

// DateHhString return current iso date and hour
// time.DateHhString() // output "20160317.10"
func DateHhString() string {
	return time.Now().Format(YMDH)
}

// DateHhMmString return current iso date and hour
// time.DateHhMmString()// output "20160317.1059"
func DateHhMmString() string {
	return time.Now().Format(YMDHM)
}

// ToDateTimeString convert time to iso date and time
// time.ToDateTimeString(time.Now()) // "2016-03-17 10:07:50"
func ToDateTimeString(t time.Time) string {
	if t == EMPTY {
		return ``
	}
	return t.Format(YMD_HMS)
}

// DateTimeString return current iso date and time
// time.DateTimeString(time.Now()) // "2016-03-17 10:07:50"
func DateTimeString() string {
	return time.Now().Format(YMD_HMS)
}

// DayInt return day of current date
func DayInt() int64 {
	return int64(time.Now().Day())
}

// HourInt return current hour
func HourInt() int64 {
	return int64(time.Now().Hour())
}

// MonthInt return current month
func MonthInt() int64 {
	return int64(time.Now().Month())
}

// YearInt return current year
func YearInt() int64 {
	return int64(time.Now().Year())
}

// YearDayInt return current day of year
func YearDayInt() int64 {
	return int64(time.Now().YearDay())
}

// IsValidTimeRange check if time in are in the range
// t1, _:=time.Parse(`1992-03-23`,T.DateFormat)
// t2, _:=time.Parse(`2016-03-17`,T.DateFormat)
// time.IsValidTimeRange(t1,t2,time.Now()) // bool(false)
func IsValidTimeRange(start, end, check time.Time) bool {
	res := check.After(start) && check.Before(end)
	return res
}

// Age returns age from current date
func Age(birthdate time.Time) float64 {
	return float64(time.Now().Sub(birthdate)/time.Hour) / 24 / 365.25
}

// AgeAt returns age from within 2 date
func AgeAt(birthdate, point time.Time) float64 {
	return float64(point.Sub(birthdate)/time.Hour) / 24 / 365.25
}

// UnixNanoAfter get current unix nano after added with certain duration
func UnixNanoAfter(d time.Duration) int64 {
	return time.Now().Add(d).UnixNano()
}

// Epoch get current unix (second) as integer
func Epoch() int64 {
	return time.Now().Unix()
}

// ToEpoch convert string date to epoch => '2019-01-01' -->1546300800
func ToEpoch(date string) int64 {
	d, err := time.Parse(YMD, date)
	if err != nil {
		return 0
	} else {
		return d.Unix()
	}
}

// EpochString return current unix (second) as string
func EpochString() string {
	return integer.Int64ToString(time.Now().Unix())
}

// EpochAfter return current unix time added with a duration
func EpochAfter(d time.Duration) int64 {
	return time.Now().Add(d).Unix()
}

// EpochAfterString return current unix time added with a duration
func EpochAfterString(d time.Duration) string {
	return integer.Int64ToString(time.Now().Add(d).Unix())
}

// convert unix time to file naming
func UnixToFile(i int64) string {
	return time.Unix(i, 0).Format(FILE)
}

// WeekdayString return day's name
func WeekdayString() string {
	return time.Now().Weekday().String()
}

// Weekday return what day is it today, Sunday => 0
func Weekday() int {
	return int(time.Now().Weekday())
}

// UnixToDateTimeString convert unix time to date time string
func UnixToDateTimeString(epoch float64) string {
	return time.Unix(int64(epoch), 0).Format(YMD_HMS)
}

// UnixToDateString convert from unix to date format string
func UnixToDateString(epoch float64) string {
	return time.Unix(int64(epoch), 0).Format(YMD)
}

// UnixToHumanDateString convert from unix to human date format
func UnixToHumanDateString(epoch float64) string {
	return time.Unix(int64(epoch), 0).Format(HUMAN_DATE)
}

// UnixToHumanString convert from unix to human format
func UnixToHumanString(epoch float64) string {
	return time.Unix(int64(epoch), 0).Format(HUMAN_DATETIME)
}

// LastTwoDigitYear return current last two digit year
func LastTwoDigitYear() string {
	return time.Now().Format(YY)
}

// TimeLoadLocation load time using specific timezone
func TimeLoadLocation(timezone string) *time.Location {
	time, _ := time.LoadLocation(timezone)
	return time
}

// CountDays - count days from 2 date input
func CountDays(layout, dateStart, dateEnd string) (int, error) {

	// Parse firstInput into datetime format //
	firstDate, err := time.Parse(layout, dateStart)
	if err != nil {
		return 0, err
	}

	// Parse secondInput into datetime format //
	secondDate, err := time.Parse(layout, dateEnd)
	if err != nil {
		return 0, err
	}

	days := secondDate.Sub(firstDate).Hours() / 24
	return int(days), err
}