// array package hold all functions related to array type
package array

import (
	"encoding/json"
	"github.com/pkg/errors"
	"strconv"
	"strings"
)

// array support package

// array (slice) of anything
//  v := array.A{}
//  v = append(v, any_value)
type A []interface{}

// array (slice) of map with string key and any value
//  v := array.MSX{}
//  v = append(v, map[string]{
//    `foo`: 123,
//    `bar`: `yay`,
//  })
type MSX []map[string]interface{}

//  ToJson convert map array of string to JSON string type
//  m, err := []interface{}{123,`abc`}
func ToJson(arr []interface{}) (string, error) {
	str, err := json.Marshal(arr)
	return string(str), errors.Wrap(err, `ToJson failed`)
}

// StringJoin combine strings in the array of string with the chosen string separator
//  m1:= []string{`satu`,`dua`}
//  array.StringJoin(m1,`-`) // satu-dua
func StringJoin(arr []string, sep string) string {
	return strings.Join(arr, sep)
}

// IntJoin combine int64s in the array of int64 with the chosen string separator
//  m1:= []int64{123,456}
//  array.IntJoin(m1,`-`) // 123-456
//func IntJoin(arr []int64, sep string) string {
//	buf := bytes.Buffer{}
//	len := len(arr) - 1
//	for idx, v := range arr {
//		buf.WriteString(I.ToS(v))
//		if idx < len {
//			buf.WriteString(sep)
//		}
//	}
//	return buf.String()
//}

// StringToInt convert string list to integer list
//  m1:= []int64{123,456}
//  array.IntJoin(m1,`-`) // 123-456
func StringToInt(arr []string) []int64 {
	res := []int64{}
	for _, v := range arr {
		if v == `` {
			continue
		}
		iv, _ := strconv.ParseInt(v, 10, 64)
		res = append(res, iv)
	}
	return res
}

// StringContains check in array of string for a value
// a := []string{"oke", "not oke"}
// m1 := array.StringContains(a, "oke") // true
func StringContains(arr []string, str string) bool {
	for _, s := range arr {
		if s == str {
			return true
		}
	}
	return false
}

// IntContains check in array of int for a value
// a := []int{1, 2}
// m1 := array.IntContains(a, 2) // true
func IntContains(arr []int, str int) bool {
	for _, s := range arr {
		if s == str {
			return true
		}
	}
	return false
}

// Int32Contains check in array of int32 for a value
// a := []int32{1, 2}
// m1 := array.Int32Contains(a, 2) // true
func Int32Contains(arr []int, str int) bool {
	for _, s := range arr {
		if s == str {
			return true
		}
	}
	return false
}

// AddStringIfNotExists add string value into slice of string if it is not exist
// a := []string{"aku", "rcti"}
// m1 := array.AddStringIfNotExists(a, "plus") // ["aku", "rcti", "plus"]
func AddStringIfNotExists(arr []string, str string) []string {
	if StringContains(arr, str) {
		return arr
	}
	return append(arr, str)
}

// AddIntIfNotExists add int value into slice of int if it is not exist
// a := []int{1, 4}
// m1 := array.AddIntIfNotExists(a, 3) // [1, 3, 4]
func AddIntIfNotExists(arr []int, str int) []int {
	if IntContains(arr, str) {
		return arr
	}
	return append(arr, str)
}

// AddInt32IfNotExists add int32 value into slice of int32 if it is not exist
// a := []int{1, 4}
// m1 := array.AddInt32IfNotExists(a, 3) // [1, 3, 4]
func AddInt32IfNotExists(arr []int, str int) []int {
	if IntContains(arr, str) {
		return arr
	}
	return append(arr, str)
}

// SliceAppendIfNotExists append slice of string from other slice if it is not exist
// a := []string{"my","name"}
// m1 := array.SliceAppendIfNotExists([]string{"jhon", "doe"}, a) // ["my", "name", "john", "doe"]
func SliceAppendIfNotExists(in, out []string) []string {
	for _, str := range in {
		if StringContains(out, str) {
			continue
		}
		out = append(out, str)
	}
	return out
}

// IsFloatExist check if value (float) exists in array
// a := []string{3,14, 1,2}
// m1 := array.IsFloatExist(a, 2,15) // false
// m2 := array.IsFloatExist(a, 1,2)  // true
func IsFloatExist(slice []float64, val float64) bool {
	for _, v := range slice {
		if val == v {
			return true
		}
	}
	return false
}

