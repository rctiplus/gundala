package integer

import (
	"strconv"
	"strings"
)

var (
	romanFig = []int64{100000, 10000, 1000, 100, 10, 1}
	romanI, romanV map[int64]rune
)

func init() {
	// M == ↀ
	romanI = map[int64]rune{1: 'I', 10: 'X', 100: 'C', 1000: 'M', 10000: 'ↂ', 100000: 'ↈ'}
	romanV = map[int64]rune{1: 'V', 10: 'L', 100: 'D', 1000: 'ↁ', 10000: 'ↇ'}
}

// simplified ternary operator (bool ? val : 0), returns second argument, if the condition (first arg) is true,
// returns 0 if not
// integer.If(true,3) // 3
// integer.If(false,3) // 0
func If(b bool, yes int64) int64 {
	if b {
		return yes
	}
	return 0
}

// IfElse ternary operator (bool ? val1 : val2), returns second argument if the condition (first arg) is true,
// third argument if not
// integer.IfElse(true,3,4) // 3
// integer.IfElse(false,3,4) // 4
func IfElse(b bool, val1, val2 int64) int64 {
	if b {
		return val1
	}
	return val2
}

// IfZero simplified ternary operator (bool ? val1==0 : val2),
// returns second argument, if val1 (first arg) is zero, returns val2 if not
// integer.IfZero(0,3) // 3
// integer.IfZero(4,3) // 4
func IfZero(val1, val2 int64) int64 {
	if val1 == 0 {
		return val2
	}
	return val1
}

// simplified ternary operator (bool ? val1==0 : val2), returns second argument,
// if val1 (first arg) is zero, returns val2 if not
// integer.IsZero(0,3) // 3
// integer.IsZero(4,3) // 4
func IsZero(val1, val2 int) int {
	if val1 == 0 {
		return val2
	}
	return val1
}

// Int64ToString convert int64 to string
// integer.Int64ToString(int64(1234)) // `1234`
func Int64ToString(num int64) string {
	return strconv.FormatInt(num, 10)
}

// IntToString convert int to string
// integer.IntToString(1234) // `1234`
func IntToString(num int) string {
	return strconv.Itoa(num)
}

// MinValue return min of two int64 values
// integer.MinValue(int64(3),int64(4)) // 3
func MinValue(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

// MaxValue return max of two int64 values
// integer.MaxValue(int64(3),int64(4)) // 4
func MaxValue(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

// MinOf return int min of two values
// integer.MinOf(3,4) // 3
func MinOf(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// MaxOf return int max of two values
// integer.MaxOf(3,4) // 4
func MaxOf(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// ToEnglishNum format ordinal number suffix such as st, nd, rd, and th.
// integer.ToEnglishNum(241)) // `241st`
// integer.ToEnglishNum(242)) // `242nd`
// integer.ToEnglishNum(244)) // `244th`
func ToEnglishNum(num int64) string {
	if num < 0 {
		return ``
	}

	prefix := Int64ToString(num)
	n2 := num % 100
	num %= 10

	if n2 == 11 || n2 == 12 || n2 == 13 {
		prefix += `th`
	} else if num == 1 {
		prefix += `st`
	} else if num == 2 {
		prefix += `nd`
	} else if num == 3 {
		prefix += `rd`
	} else {
		prefix += `th`
	}

	return prefix
}

// PadZero converts int64 (first arg) to string with zero padded with maximum length
// integer.PadZero(123,5) // `00123`
func PadZero(num int64, length int) string {
	str := Int64ToString(num)
	sLen := len(str)
	if sLen >= length {
		return str
	}

	return strings.Repeat(`0`, length-sLen) + str
}

// convert int64 to roman number
// integer.ToRoman(16)) // output "XVI"
func ToRoman(num int64) string {
	res := []rune{}
	x := ' '
	for _, z := range romanFig {
		digit := num / z
		i, v := romanI[z], romanV[z]
		switch digit {
		case 1:
			res = append(res, i)
		case 2:
			res = append(res, i, i)
		case 3:
			res = append(res, i, i, i)
		case 4:
			res = append(res, i, v)
		case 5:
			res = append(res, v)
		case 6:
			res = append(res, v, i)
		case 7:
			res = append(res, v, i, i)
		case 8:
			res = append(res, v, i, i, i)
		case 9:
			res = append(res, i, x)
		}
		num -= digit * z
		x = i
	}
	return string(res)
}
