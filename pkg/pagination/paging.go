package pagination

import "math"

// CountTotalPage count total page from datas and show perpage
func CountTotalPage(count int64, show int) int {
	d := float64(count) / float64(show)
	page := int(math.Ceil(d))
	return int(page)
}
