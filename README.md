# Gundala #

![alternativetext](doc/gundala.png)

* [Description](#description)
* [Installation](#installation)
* [Usage](#usage)
    + [Datatype/array package](#making-a-simple-get-request)
    + [Datatype/char package](#creating-a-hystrix-like-circuit-breaker)
    + [Datatype/integer package](#creating-a-hystrix-like-circuit-breaker)
    + [Datatype/string package](#creating-a-hystrix-like-circuit-breaker)
    + [Datatype/time package](#creating-a-hystrix-like-circuit-breaker)
    + [Encoder package](#creating-a-hystrix-like-circuit-breaker)
    + [Mail package](#creating-a-hystrix-like-circuit-breaker)
    + [Pagination package](#creating-a-hystrix-like-circuit-breaker)
* [Plugins](#plugins)
* [Documentation](#documentation)
* [FAQ](#faq)
* [License](#license)

### Description ###

Gundala is a set of library used to help on developing application using Go :

- datatype/array package (set of functions for array processing).
- datatype/char package (set of functions for char processing).
- datatype/integer package (set of functions for array processing).
- datatype/string package (set of functions for string processing).
- datatype/time package (set of functions for time processing).
- encoder package (set of functions for encode and decode).
- mail package (set of functions for send mail).
- pagination package (set of functions for pagination).

### Importing the package
Before importing gundala into your code, first download gundala using `go get` tool.

```bash
$ go get bitbucket.org/rctiplus/gundala 
```

To use the gundala, simply adding the following import statement to your `.go` files.

```go
import "bitbucket.org/rctiplus/gundala/pkg/datatype/time" // with go modules disabled
```

## Documentation

Further documentation can be found on [pkg.go.dev](https://pkg.go.dev/bitbucket.org/rctiplus/gundala)

## FAQ

**Can I contribute to make Gundala?**

[Please do!](https://bitbucket.org/rctiplus/gundala/blob/master/CONTRIBUTING.md) We are looking for any kind of contribution to improve Gundala core funtionality and documentation. When in doubt, make a PR!

## License

```
Copyright (c) 2020, RCTIPlus

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```